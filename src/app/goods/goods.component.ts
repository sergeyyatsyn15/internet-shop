import {Component, Input, OnInit} from '@angular/core';
import {GoodsModel} from '../models/goods.model';

@Component({
  selector: 'app-goods',
  templateUrl: './goods.component.html',
  styleUrls: ['./goods.component.css']
})
export class GoodsComponent implements OnInit {
  @Input() good: GoodsModel;

  constructor() {
  }

  ngOnInit() {
    console.log(this.good);
  }

}
