import {Component, Input, OnInit} from '@angular/core';
import { ClientService } from '../services/client-service';
import {ClientModel} from '../models/client.model';
import {MatDialogModule, MatDialogRef} from '@angular/material';
import {DialogService} from '../services/dialog.service';
import {GoodsModel} from '../models/goods.model';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  @Input() goodsInBasket: GoodsModel[] = [];
  clients: ClientModel[] = [];
  constructor(private clientService: ClientService,
              public dialogService: DialogService) {
  }

  ngOnInit() {
    this.clientService.getAllClients().subscribe(clients => {
      this.clients = clients;
      console.log(this.clients);
    });

  }

  openBasket() {
    this.dialogService.openConfirmDialog(this.goodsInBasket);
  }
}
