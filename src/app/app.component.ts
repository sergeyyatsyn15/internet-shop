import {Component, OnInit} from '@angular/core';
import {GoodsModel} from './models/goods.model';
import {ClientModel} from './models/client.model';
import {GoodsService} from './services/goods-service';
import {BasketService} from './services/basket.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'internet-shop';
  goods: GoodsModel[] = [];
  clients: ClientModel[] = [];

  // goodsInBasket: GoodsModel[] = [];

  constructor(private goodsService: GoodsService,
              private basketService: BasketService) {

  }

  ngOnInit(): void {
    this.goodsService.getAllGoods().subscribe(goods => this.goods = goods);
  }

  // putGoodsIntoBasket(goods: GoodsModel) {
  //   this.goodsInBasket.push(goods);
  // }
}
