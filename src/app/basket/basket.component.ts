import {Component, EventEmitter, Inject, OnInit, Output} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';
import {GoodsModel} from '../models/goods.model';
import {BasketService} from '../services/basket.service';

@Component({
  selector: 'app-basket',
  templateUrl: './basket.component.html',
  styleUrls: ['./basket.component.css']
})
export class BasketComponent implements OnInit {


  constructor(@Inject(MAT_DIALOG_DATA) public data: GoodsModel[],
              private basketService: BasketService) {
  }

  ngOnInit() {
  }

  deleteFromBasket(goods: GoodsModel) {
    this.basketService.deleteGoodsFromBasket(goods);
  }

  getTotalPrice() {
    return this.data.map(goods => goods.price).reduce((acc, price) => {
      return acc + price;
    });
  }

  buyGoodsIntoBasket() {

  }
}
