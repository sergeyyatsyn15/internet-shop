import {Injectable} from '@angular/core';
import {MatDialog} from '@angular/material';
import {BasketComponent} from '../basket/basket.component';
import {GoodsModel} from '../models/goods.model';

@Injectable({
  providedIn: 'root'
})
export class BasketService {
  goodsInBasket: GoodsModel[] = [];

  constructor() {
  }

  addGoodToBasket(goods: GoodsModel): void {
    this.goodsInBasket.push(goods);
  }

  deleteGoodsFromBasket(goods: GoodsModel): void {
    this.goodsInBasket.splice(this.goodsInBasket.indexOf(goods), 1);
  }
}
