import {Injectable} from '@angular/core';
import {MatDialog} from '@angular/material';
import {BasketComponent} from '../basket/basket.component';

@Injectable({
  providedIn: 'root'
})
export class DialogService {

  constructor(private dialog: MatDialog) {
  }

  openConfirmDialog(data) {
    this.dialog.open(BasketComponent, {
      width: '90%',
      height: '90%',
      data
    });
  }
}
