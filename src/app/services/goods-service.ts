import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {GoodsModel} from '../models/goods.model';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class GoodsService {

  constructor(
    private httpClient: HttpClient
  ) {
  }

  getAllGoods(): Observable<GoodsModel[]> {
    return this.httpClient.get<GoodsModel[]>(environment.backend + 'api/goods', {headers: environment.headers});
  }
}
