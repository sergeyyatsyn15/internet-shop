import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {Observable} from 'rxjs';
import {ClientModel} from '../models/client.model';

@Injectable({
  providedIn: 'root',
})
export class ClientService {

  constructor(
    private httpClient: HttpClient
  ) {
  }

  getAllClients(): Observable<ClientModel[]> {
    return this.httpClient.get<ClientModel[]>(environment.backend + 'api/clients', {headers: environment.headers});
  }
}
