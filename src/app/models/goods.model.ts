export interface GoodsModel {
  id: number;
  name: string;
  price: number;
  priceMeasure: string;
  availableAmount: number;
  imageUrl: string;
}
