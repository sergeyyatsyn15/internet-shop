export interface ClientModel {
  id: number;
  firstName: string;
  lastName: number;
  address: string;
  middleName: number;
  phone: string;
  email: string;
  regular: string;
}
