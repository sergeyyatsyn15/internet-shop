import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {GoodsService} from '../services/goods-service';
import {GoodsModel} from '../models/goods.model';
import {BasketService} from '../services/basket.service';

@Component({
  selector: 'app-goods-list',
  templateUrl: './goods-list.component.html',
  styleUrls: ['./goods-list.component.css']
})
export class GoodsListComponent implements OnInit {
  @Input() goods: GoodsModel[] = [];

  // @Output() putGoodsIntoBasketEvent = new EventEmitter<GoodsModel>();

  constructor(
    private goodsService: GoodsService,
    private basketService: BasketService
  ) {
  }

  ngOnInit() {
    // this.goodsService.getAllGoods()
    //   .subscribe(goods => this.goods = goods);
  }

  putGoodsIntoBasket(good: GoodsModel) {
    // this.putGoodsIntoBasketEvent.emit(good);
    this.basketService.addGoodToBasket(good);
  }
}
